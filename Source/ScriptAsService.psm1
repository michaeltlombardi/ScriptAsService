Get-ChildItem "$PSScriptRoot\Functions" -Recurse `
| Where-Object -FilterScript {$_.FullName -match "\.ps1$" -and $_.FullName -notmatch "\.Tests\."} `
| ForEach-Object -Process {
    . $_.FullName
}

Export-ModuleMember -Function *