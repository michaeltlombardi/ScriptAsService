$PSModuleAutoLoadingPreference = "None"

Given 'the ScriptAsService module is imported' {
    $Version = (Get-Module -ListAvailable $env:BHPSModuleManifest).Version
    Import-Module $env:BHProjectPath\$env:BHProjectName\$Version\$env:BHProjectName.psd1 -Force
}

Given 'a script with looping logic' {
    @"
While ($true) {
    Get-Date
    Start-Sleep -Seconds 10
}
"@ | Out-File (Resolve-Path "$env:TEMP/ScriptAsService.ps1")
}

When 'I call New-ScriptAsService' {
    New-ScriptAsService -Path "$env:TEMP/ScriptAsService.ps1" -Destination "$env:TEMP/ScriptAsService.exe" -Name Script -DisplayName "Script As Service"
}

Then 'an exe is created' {
    Test-Path "$env:TEMP/ScriptAsService.exe" | Should Be $true
}