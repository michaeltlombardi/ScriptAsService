#Requires -Module Pester,BuildHelpers
$env:BHProjectPath
Describe "Project $env:BHProjectName Meta Documents" {
    Context "README" {
        It "Exists" {
            Test-Path $env:BHProjectPath\README.md | Should Be $true
        }
        It "Links to the Latest Entry in the Changelog" {
            # Select the link text from the version in the README
            $ReadMeVersionLink = (Get-Content $env:BHProjectPath\README.md)[0].Split('#')[2] `
            | ForEach-Object {$_.Substring(0,($_.length -1))}
            # Grab the latest changelog entry and parse for the link text
            $LatestChangeLogLink = (Get-Content $env:BHProjectPath\CHANGELOG.md) -match "^##\s\[\d+\." `
            | Select-Object -First 1 `
            | ForEach-Object {$_.Substring(4).replace(']','').replace('.','').replace(' ','')}
            $ReadMeVersionLink | Should Be $LatestChangeLogLink
        }
    }
    Context "LICENSE" {
        It "Exists" {
            Test-Path $env:BHProjectPath\LICENSE.md | Should Be $true
        }
    }
    Context "CONTRIBUTING" {
        It "Exists" {
            Test-Path $env:BHProjectPath\CONTRIBUTING.md | Should Be $true
        }
    }
    Context "CHANGELOG" {
        It "Exists" {
            Test-Path $env:BHProjectPath\CHANGELOG.md | Should Be $true
        }
        It "Includes an 'Unreleased' Section" {
            (Get-Content $env:BHProjectPath\CHANGELOG.md) -match '^## \[Unreleased\]$' | Should Not BeNullOrEmpty
        }
        It "Includes the latest module release" {
            $ModuleVersion = (Get-Module -ListAvailable $env:BHPSModuleManifest).Version
            # Grab the latest changelog entry and parse for the version number
            $LatestChangeLogVersion = (Get-Content $env:BHProjectPath\CHANGELOG.md)  -match "^##\s\[\d+\." `
            | Select-Object -First 1 `
            | ForEach-Object {$_.Substring(4).split(']')[0]}
            $LatestChangeLogVersion | Should Be $ModuleVersion
        }
    }
}