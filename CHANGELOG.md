# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

**NOTE:** _Until this project goes to version `1.0.0` it is considered to be in beta.

## [Unreleased]
## [0.1.3] - 2017-04-18
### Modified
+ Manifest to be properly formatted and defined so as to allow for publishing to the gallery.
+ Updated the `Deploy` task in the psake file to cause the deployment to be verbose in CI.

## [0.1.2] - 2017-04-18
### Modified
+ Manifest to reflect nested modules for the Sorlov Assemblies instead of loading the modules in the PSM1.
+ PSM1 for the same reason
+ Removed extraneous line copies from the build function to reflect these changes.

## [0.1.1] - 2017-04-18
### Added
+ Initial module, relying on Sorlov Assemblies
+ MIT License
+ Initial README
+ Example Scripts
+ Initial CONTRIBUTING document exlaining how to help work on the project
+ Initial build + GitLab CI